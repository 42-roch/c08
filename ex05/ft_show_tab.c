/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/13 16:55:40 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/13 17:36:46 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>
#include "ft_stock_str.h"

void	printchar(char a)
{
	write(1, &a, 1);
}

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		++i;
	write(1, str, i);
}

void	printnbr(int nb)
{
	unsigned int	nbr;

	if (nb < 0)
	{
		printchar('-');
		nbr = nb * -1;
	}
	else
		nbr = nb;
	if (nbr >= 10)
		printnbr(nbr / 10);
	printchar(nbr % 10 + 48);
}

void	ft_show_tab(struct s_stock_str *par)
{
	int	i;

	i = 0;
	while (par[i].str != '\0')
	{
		ft_putstr(par[i].str);
		printchar('\n');
		printnbr(par[i].size);
		printchar('\n');
		ft_putstr(par[i].copy);
		printchar('\n');
		i++;
	}
}

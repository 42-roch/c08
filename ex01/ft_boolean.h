/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_boolean.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/13 13:46:22 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/14 09:32:54 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BOOLEAN_H
# define FT_BOOLEAN_H

# include <unistd.h>
# define SUCCESS	0

# define TRUE	1
# define FALSE	0

# define EVEN_MSG	"I have an odd number of arguments.\n"
# define ODD_MSG	"I have an even number of arguments.\n"

typedef int	t_bool;

# define EVEN(var) (var % 2)
#endif
